<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Task;
use yii\web\NotFoundHttpException;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $data = Task::find()
            ->andWhere(['parent'=>null])
            ->andWhere(['<','status', Task::TASK_STATUS_DELETED])
            ->orderBy(['id'=>SORT_DESC])->all();

        return $this->render('index',[
            'data'=>$data
        ]);
    }

    /**
     * @return string
     */
    public function actionArchive()
    {
        $models = Task::find()
            ->andWhere(['parent'=>null])
            ->andWhere(['>','status', Task::TASK_STATUS_ACTIVE])
            ->orderBy(['id'=>SORT_DESC])->all();

        return $this->render('archive',[
            'models'=>$models
        ]);
    }

    /**
     * @param null $parent
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxTaskAdd($parent=null){
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = new Task();

        if(isset($parent))
            $model->parent = $parent;

        return json_encode($model->save());
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxTaskDelete($id){
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = Task::findOne($id);

        if(!isset($model))
            throw new NotFoundHttpException();

        return json_encode($model->setDeleted());
    }

    /**
     * @param $id
     * @param $text
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxTaskUpdate($id, $text){
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = Task::findOne($id);

        if(!isset($model))
            throw new NotFoundHttpException();

        return json_encode($model->setText($text));
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAjaxTaskDone($id){
        if (!\Yii::$app->request->isAjax)
            throw new NotFoundHttpException();

        $model = Task::findOne($id);

        if(!isset($model))
            throw new NotFoundHttpException();

        return json_encode($model->switchDone());
    }
}
