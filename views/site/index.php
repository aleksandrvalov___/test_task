<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'my task';
?>
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">

                <section class="content-header">
                    <h1>
                        <small>my task</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=Url::toRoute(['/site/index'])?>">ToDo</a></li>
                        <li><a href="<?=Url::toRoute(['/site/archive'])?>"> Archive</a></li>
                    </ol>
                </section>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">ToDo List</h3>
                        <div class="box-tools pull-right">
                            <a  href="javascript:void(0)" class="task_add_button"><i class="fa fa-2x fa-plus-circle"></i></a>
                        </div>
                    </div>
                    <div class="box-body">
                        <?php Pjax::begin(['id'=>'task_list']); ?>
                        <?php foreach($data as $item){ ?>
                            <?= $this->render("_task_item", ["item"=>$item])?>
                        <?php } ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>

