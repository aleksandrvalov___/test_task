<?php

use yii\db\Migration;

class m170603_134508_create_tbl_task extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('tbl_task', [
            'id' => $this->primaryKey(),
            'description' => $this->text(),
            'status' => $this->integer()->defaultValue(0),
            'parent' => $this->integer(),
            'created_at' => $this->integer()->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('tbl_task');
    }

}
