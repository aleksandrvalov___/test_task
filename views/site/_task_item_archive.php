<?php
use app\models\Task;
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <small class="text-primary"><?=date("H:i:s", $item->created_at)?></small>
            <?php if($item->status != Task::TASK_STATUS_DELETED){ ?>
                <a href="javascript:void(0)" class="task_delete_button text-danger" data-id="<?=$item->id?>">
                    <i class="fa fa-trash"></i>
                </a>
            <?php }else{ ?>
                <label class="label label-danger">deleted</label>
            <?php } ?>

        <p class="text-muted"><?=$item->description?></p>
    </div>
</div>
<div class="row">
    <div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-12">
        <?php foreach ($item->sub as $item_sub){ ?>
            <?=$this->render('_task_item_archive', ['item'=>$item_sub])?>
        <?php } ?>
    </div>
</div>

<?php if(!isset($item->top)){ ?>
    <hr>
<?php } ?>

