<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 03.06.2017
 * Time: 22:43
 */

use app\models\Task;
?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="form-group">
       <small class="text-primary"><?=date("H:i:s", $item->created_at)?></small>
            <a href="javascript:void(0)" class="task_done_button " data-id="<?=$item->id?>">
                <i class="fa <?=($item->status == Task::TASK_STATUS_ACTIVE)?"text-success fa-arrow-circle-down":"text-warning fa-arrow-circle-up"?>"></i>
            </a>
            <?php if($item->status == Task::TASK_STATUS_ACTIVE){ ?>
                <a href="javascript:void(0)" style="padding-left: 3px; padding-right: 3px;" class="task_add_button text-primary" data-parent="<?=$item->id?>">
                    <i class="fa fa-plus-square"></i>
                </a>
            <?php } ?>
            <a href="javascript:void(0)" class="task_delete_button text-danger" data-id="<?=$item->id?>">
                <i class="fa fa-trash"></i>
            </a>

            <?php if($item->status == Task::TASK_STATUS_ACTIVE){ ?>
        <textarea placeholder="Your task description.." class="form-control task_description" data-id="<?=$item->id?>"><?=$item->description?></textarea>
            <?php }else{ ?>
                <p class="text-success task_done"><?=$item->description?></p>
            <?php } ?>
        </div>
    </div>
</div>
    <div class="row">
    <div class="col-lg-11 col-lg-offset-1 col-md-11 col-md-offset-1 col-sm-12">
        <?php foreach ($item->subActive as $item_sub){ ?>
            <?=$this->render('_task_item', ['item'=>$item_sub])?>
        <?php } ?>
    </div>
    </div>

<?php if(!isset($item->top)){ ?>
<hr>
<?php } ?>

