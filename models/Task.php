<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * Class Task
 * @package app\models
 */
class Task extends \yii\db\ActiveRecord
{

    const TASK_STATUS_ACTIVE = 0;
    const TASK_STATUS_DONE = 1;
    const TASK_STATUS_DELETED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_task';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdocs
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['status', 'parent', 'created_at'], 'integer'],
        //    [['created_at'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Задача',
            'status' => 'Status',
            'parent' => 'Parent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSub()
    {
        return $this->hasMany(Task::className(), ['parent' => 'id']);
    }

    /**
     * @return $this
     */
    public function getSubActive()
    {
        return $this->hasMany(Task::className(), ['parent' => 'id'])->andWhere(['<','status', Task::TASK_STATUS_DELETED]);
    }

    /**
     * @return int|string
     */
    public function getSubActiveCount()
    {
        return $this->hasMany(Task::className(), ['parent' => 'id'])->andWhere(['status'=>self::TASK_STATUS_ACTIVE])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTop()
    {
        return $this->hasOne(Task::className(), ['id' => 'parent']);
    }

    /**
     * @return bool
     */
    public function setDeleted(){
        $this->status = self::TASK_STATUS_DELETED;
        return $this->save();
    }

    /**
     * @return bool
     */
    public function setDone(){
        if($this->status == Task::TASK_STATUS_ACTIVE){
            $this->status = self::TASK_STATUS_DONE;
            return $this->save();
       }
        return true;
    }

    /**
     * @return bool
     */
    public function setActive(){
        if($this->status == Task::TASK_STATUS_DONE){
            $this->status = self::TASK_STATUS_ACTIVE;
            return $this->save();
        }
        return true;
    }

    /**
     * @return bool
     */
    public function switchDone(){
        $this->status = ($this->status == self::TASK_STATUS_ACTIVE)?self::TASK_STATUS_DONE:self::TASK_STATUS_ACTIVE;
        return $this->save();
    }

    /**
     * @param $text
     * @return bool
     */
    public function setText($text){
        $this->description = $text;
        return $this->save();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if(isset($changedAttributes['status'])){
            if($this->status == Task::TASK_STATUS_DONE){
                if(isset($this->top) && $this->top->subActiveCount == 0){
                    $this->top->setDone();
                }
                foreach($this->subActive as $sub){
                    $sub->setDone();
                }//
            }else if($this->status == Task::TASK_STATUS_ACTIVE){
                if(isset($this->top)){
                    $this->top->setActive();
                }
            }//
        }//
    }
}
