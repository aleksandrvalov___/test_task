$(document).ready(function(){

    var bind = function(){
        $(".task_add_button").unbind("click");
        $(".task_add_button").bind("click", function(){
            var elem = this;
            var data = { parent: $(elem).attr("data-parent")};
            console.log(data);
            $.ajax({
                method: "GET",
                url: "/site/ajax-task-add",
                data: data
            })
                .done(function( msg ) {
                    $.pjax.reload({container:'#task_list'});
                });

        });

        $(".task_done_button").unbind("click");
        $(".task_done_button").bind("click", function(){
            var elem = this;
            var data = { id: $(elem).attr("data-id")};
            console.log(data);
            $.ajax({
                method: "GET",
                url: "/site/ajax-task-done",
                data: data
            })
                .done(function( msg ) {
                    $.pjax.reload({container:'#task_list'});
                });

        });

        $(".task_delete_button").unbind("click");
        $(".task_delete_button").bind("click", function(){
            var elem = this;
            var data = { id: $(elem).attr("data-id")};
            console.log(data);
            $.ajax({
                method: "GET",
                url: "/site/ajax-task-delete",
                data: data
            })
                .done(function( msg ) {
                    $.pjax.reload({container:'#task_list'});
                });

        });

        $(".task_description").unbind("keyup");
        $(".task_description").bind("keyup", function(){
            var elem = this;
            var data = { id: $(elem).attr("data-id"), text: $(elem).val()};
            console.log(data);
            $.ajax({
                method: "GET",
                url: "/site/ajax-task-update",
                data: data
            })
                .done(function( msg ) {
                    console.log(msg);
                    //$.pjax.reload({container:'#task_list'});
                });

        });
    }//

    bind();

    $("#task_list").on("pjax:end", function() {
        bind();
    });

});