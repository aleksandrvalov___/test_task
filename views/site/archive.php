<?php

use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'my task';
?>

<div class="row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
        <section class="content-header">
            <h1>
                <small>archive task</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?=Url::toRoute(['/site/index'])?>">ToDo</a></li>
                <li><a href="<?=Url::toRoute(['/site/archive'])?>"> Archive</a></li>
            </ol>
        </section>
        <div class="box box-primary">
            <div class="box-body">
                <?php Pjax::begin(['id'=>'task_list']); ?>
                <?php foreach($models as $item){ ?>
                    <?= $this->render("_task_item_archive", ["item"=>$item])?>
                <?php } ?>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
</div>

